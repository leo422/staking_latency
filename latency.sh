########################################################################################################################
###    This Script will ping the AWS servers in francfurt that are the current center of the INT network.
###
###    I will apply a chrony value equal to half the return trip to match the time delay of your block ariving to the hub
### 
###    After applying the initial value it will ping continuisly and if the delay is our of rage for 1min will apply a new chrony value
###    This will help out situations where ISPs are overloaded and latency spikes for long periods 
###  
###    I do not reccomend using harder values sinse changing the chrony delay to offten can have a negative impact on your pools performance 
########################################################################################################################
FLEX=5                   ## the ammount of flexibility you want to have on your latency default it 5ms
RETRY_LIMIT=5             ## the number of failed checks before you update the chrony values check time = (5 sec sleep + ~5 sec ping) ~ 10 sec

## initialization
currern_average=0
flex=20
retries=0


apply_crony_latency (){
    half_trip_time="$(( $currern_average / 2 -2  ))" # Removeing 2 ms from the latency 
    secs=`echo "scale=4;${half_trip_time}/1000" | bc`
    echo "applying chrony latency $secs "
#    python3 telegram_send.py "applying chrony latency $secs "   #Uncomment this line if you want to get Telegram notifications when chrony is updated 
    sed  "s/LATENCY/$secs/g" chrony.conf.template > chrony.conf
    mv chrony.conf /etc/chrony.conf
    systemctl restart chronyd

}



## main loop
while sleep 5; do
	t="$(ping -c 10 3.120.0.0 | tail -n 1)" 
  echo "$t"
  IFS='/'
  read -ra ADDR <<< "$t"
  ping_avrg="${ADDR[4]%.*}"
  echo "Average ping: $ping_avrg"
  drift="$(echo $(( $ping_avrg - $currern_average )) | sed 's/-//')"  
  echo "Average drift: $drift "
  if [ $drift -gt $FLEX  ]
  then
    echo "Warning Drift over the limit retry : $retries"	  
    retries="$(( retries + 1 ))"
    if [ $retries -gt $RETRY_LIMIT  ] || [ $currern_average == 0 ]
    then
       echo "Warning updateing Crony latency for $ping_avrg"
       currern_average="$ping_avrg"
       apply_crony_latency
       
    fi	    
  else 
    retries=0 
  fi 
done
