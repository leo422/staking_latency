# Staking pool latency ajustment tool


This Script will ping the AWS servers in francfurt that are the current center of the INT network.

It will apply a chrony value equal to half the return trip to match the time delay of your block ariving to the hub
 
After applying the initial value it will ping continuisly and if the delay is our of rage for 1min will apply a new chrony value
This will help out situations where ISPs are overloaded and latency spikes for long periods 
  
I do not reccomend using harder values sinse changing the chrony delay to offten can have a negative impact on your pools performance 


To use the Script you need to create a "chrony.conf.template" file based on your currnet system options
you can do this using:
> cp /etc/chrony.conf chrony.conf.template


Then using your favorite editor open the file and `offset LATENCY` to all the pool listings

Example:
> pool time.google.com       iburst minpoll 1 maxpoll 2 maxsources 3 offset LATENCY


Incluted is a script that will send telegram notification when chrony is updated. 

You can use with an existing chatbot or you can create a new one from the bot master.

To enable uncomment line 13 from the script

